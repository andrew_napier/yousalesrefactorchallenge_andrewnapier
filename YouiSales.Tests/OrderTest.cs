﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Shouldly;

namespace YouiSales.Tests
{
    [TestFixture]
    public class OrderTest
    {
        private readonly static Policy BMW = new Policy("Jane Doe", "BMW", PolicyKind.Car);
        private readonly static Policy Harley = new Policy("John Doe", "Harley", PolicyKind.Motorcycle);
        private readonly static Policy SunnyCoast = new Policy("John Doe", "Sunshine Coast", PolicyKind.Home);
        private readonly DateTime orderDate = new DateTime(2019,10,25,9,7,27, DateTimeKind.Unspecified);
        private readonly IDictionary<PolicyKind, PolicyBusinessLogic> policyPortfolio 
            = new Dictionary<PolicyKind, PolicyBusinessLogic>();

        [SetUp]
        public void Setup()
        {
            policyPortfolio.Clear();
            policyPortfolio.Add(PolicyKind.Car,
                new PolicyBusinessLogic { DiscountRate = 0.9M, UnitPrice = 105M });
            policyPortfolio.Add(PolicyKind.Home,
                new PolicyBusinessLogic { DiscountRate = 0.8M, UnitPrice = 235M });
            policyPortfolio.Add(PolicyKind.Motorcycle,
                new PolicyBusinessLogic { DiscountRate = 0.8M, UnitPrice = 56M });
        }

        [Test]
        public void ReceiptOneBMW()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            order.AddLine(new Line(BMW, 1));
            order.Receipt().ShouldBe(ResultStatementOneBMW);
        }

        private const string ResultStatementOneBMW = @"Order Receipt for Youi
	1 x Jane Doe BMW = $105.00
Sub-Total: $105.00
Tax: $10.50
Total: $115.50
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneHarley()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            order.AddLine(new Line(Harley, 1));
            order.Receipt().ShouldBe(ResultStatementOneHarley);
        }

        private const string ResultStatementOneHarley = @"Order Receipt for Youi
	1 x John Doe Harley = $56.00
Sub-Total: $56.00
Tax: $5.60
Total: $61.60
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void ReceiptOneSunnyCoast()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            order.AddLine(new Line(SunnyCoast, 1));
            order.Receipt().ShouldBe(ResultStatementOneSunnyCoast);
        }

        private const string ResultStatementOneSunnyCoast = @"Order Receipt for Youi
	1 x John Doe Sunshine Coast = $235.00
Sub-Total: $235.00
Tax: $23.50
Total: $258.50
Date: Friday, 25 October 2019 9:07:27 AM";

        [Test]
        public void HtmlReceiptOneBMW()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            order.AddLine(new Line(BMW, 1));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneBMW);
        }

        private const string HtmlResultStatementOneBMW = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe BMW = $105.00</li></ul><h3>Sub-Total: $105.00</h3><h3>Tax: $10.50</h3><h2>Total: $115.50</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneHarley()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            order.AddLine(new Line(Harley, 1));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneHarley);
        }

        private const string HtmlResultStatementOneHarley = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x John Doe Harley = $56.00</li></ul><h3>Sub-Total: $56.00</h3><h3>Tax: $5.60</h3><h2>Total: $61.60</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void HtmlReceiptOneSunnyCoast()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            order.AddLine(new Line(SunnyCoast, 1));
            order.HtmlReceipt().ShouldBe(HtmlResultStatementOneSunnyCoast);
        }

        private const string HtmlResultStatementOneSunnyCoast = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x John Doe Sunshine Coast = $235.00</li></ul><h3>Sub-Total: $235.00</h3><h3>Tax: $23.50</h3><h2>Total: $258.50</h2><h3>Date: Friday, 25 October 2019 9:07:27 AM</h3></body></html>";

        [Test]
        public void OrderShouldFailAddingNullLine()
        {
            var order = new Order("Youi", orderDate, policyPortfolio);
            TestDelegate subject = () => order.AddLine(null);

            Assert.That(subject, Throws.TypeOf(typeof(ArgumentNullException)));
        }

        [Test]
        public void LineConstructionShouldFailWithInvalidParameters()
        {
            TestDelegate subject = () => new Line(null, 3);
            Assert.That(subject, Throws.TypeOf(typeof(ArgumentNullException)));

            subject = () => new Line(Harley, 0);
            Assert.That(subject, Throws.TypeOf(typeof(ArgumentException)));
        }
    }
}
