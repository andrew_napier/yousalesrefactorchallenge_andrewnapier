﻿
namespace YouiSales
{
    public class Policy
    {
        public Policy(string policyHolderName, string description, PolicyKind kind)
        {
            PolicyHolderName = policyHolderName;
            Description = description;
            Kind = kind;
        }

        public string PolicyHolderName { get; set; }
        public string Description { get; set; }
        public PolicyKind Kind { get; set; }

    }
}
