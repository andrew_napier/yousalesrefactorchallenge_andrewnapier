﻿using System;

namespace YouiSales
{
    /// <summary>
    /// PolicyBusinessLogic defines how to build the price for an insurance line.
    /// There are discounts involved, when purchasing quantities of a policy
    /// equal to or greater than the QtyBasedDiscountTrigger.
    /// </summary>
    public class PolicyBusinessLogic
    {
        public int QtyBasedDiscountTrigger { get; set; } = 2;
        public decimal UnitPrice { get; set; }

        public decimal DiscountRate { get; set; }

        public decimal GetPrice(Line line)
        {
            if (line is null)
            {
                throw new ArgumentNullException(nameof(line));
            }

            if (line.Quantity >= QtyBasedDiscountTrigger)
            {
                return line.Quantity * UnitPrice * DiscountRate;
            }

            return line.Quantity * UnitPrice;
        }
    }
}
