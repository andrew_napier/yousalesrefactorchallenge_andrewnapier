﻿
namespace YouiSales
{
    /// <summary>
    /// Specifies types of insurance policies offerred.
    /// A policy-portfolio should include a 1:1 mapping of
    /// policy types to PolicyBusinessLogic instances.
    /// </summary>
    public enum PolicyKind 
    {
        Car,
        Motorcycle,
        Home
    } 
}
