﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YouiSales
{
    public class Order
    {
        private const decimal taxRate = .1M;
        private readonly IList<Line> lines = new List<Line>();
        private readonly IDictionary<PolicyKind, PolicyBusinessLogic> portfolio;

        public Order(
            string company, 
            DateTime orderDate,
            IDictionary<PolicyKind, PolicyBusinessLogic> policyPortfolio)
        {
            Company = company;
            OrderDate = orderDate;
            portfolio = policyPortfolio;
        }

        public string Company { get; }
        public DateTime OrderDate { get; }

        public void AddLine(Line line)
        {
            lines.Add(line  ?? throw new ArgumentNullException(nameof(line)));
        }

        public string Receipt()
        {
            Logger.Instance.LogInformation("Printing receipt (text version) - Start");

            var result = GenerateReceipt(new TextOrderFormatter());

            Logger.Instance.LogInformation("Printing receipt (text version) - Finish");

            return result;
        }

        private string GenerateReceipt(IOrderFormatter formatter)
        {
            var totalAmount = 0M;
            formatter.AddReceiptPrefix(Company);
            if (lines.Any())
            {
                formatter.AddLineItemPrefix();
                for (var index = 0; index < lines.Count; index++)
                {
                    var line = lines[index];
                    var thisAmount = GetLineValue(line);

                    formatter.AddLineItem(line, thisAmount);
                    totalAmount += thisAmount;
                }

                formatter.AddLineItemSuffix();
            }

            var tax = totalAmount * taxRate;
            formatter.AddReceiptSuffix(totalAmount, tax, OrderDate);

            return formatter.ToString();
        }

        private decimal GetLineValue(Line line)
        {
            if (!portfolio.ContainsKey(line.Policy.Kind))
            {
                throw new ArgumentOutOfRangeException($"Policy kind not found in portfolio: {line.Policy.Kind}");
            }

            var policyBusinessLogic = portfolio[line.Policy.Kind];
            return policyBusinessLogic.GetPrice(line);
        }

        public string HtmlReceipt()
        {
            Logger.Instance.LogInformation("Printing receipt (HTML version) - Start");

            var result = GenerateReceipt(new HtmlOrderFormatter());

            Logger.Instance.LogInformation("Printing receipt (HTML version) - Finish");

            return result;
        }

    }
}