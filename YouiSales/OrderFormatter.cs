﻿using System;

namespace YouiSales
{
    /// <summary>
    /// Implement this interface to provide alternate text rendering of an order.
    /// The order will render as follows:
    /// - Receipt Prefix
    ///   - Line Item Prefix
    ///   - Line Item (1..n)
    ///   - Line Item Suffix
    /// - Receipt Suffix
    /// </summary>
    interface IOrderFormatter
    {
        void AddReceiptPrefix(string company);
        void AddLineItemPrefix();
        void AddLineItem(Line line, decimal amount);
        void AddLineItemSuffix();
        void AddReceiptSuffix(decimal totalAmount, decimal tax, DateTime orderDate);

        string ToString();
    }

}
