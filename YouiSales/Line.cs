﻿using System;

namespace YouiSales
{
    public class Line
    {
        public Line(Policy policy, int quantity)
        {
            Policy = policy  ?? throw new ArgumentNullException(nameof(policy));
            Quantity = quantity != 0 
                ? quantity 
                : throw new ArgumentException($"{nameof(quantity)} cannot be 0");
        }

        public Policy Policy { get; set; }
        public int Quantity { get; set; }
    }
}