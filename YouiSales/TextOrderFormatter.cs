﻿using System;
using System.Text;

namespace YouiSales
{
    /// <summary>
    /// Create an instance of this class to render the Order in plain-text format.
    /// </summary>
    public class TextOrderFormatter : IOrderFormatter
    {
        private const string LineItemFormat = "\t{0} x {1} {2} = {3}";
        private const string ReceiptPrefixFormat = "Order Receipt for {0}{1}";
        private const string SubtotalFormat = "Sub-Total: {0}";
        private const string TaxFormat = "Tax: {0}";
        private const string OrderTotalFormat = "Total: {0}";
        private const string OrderDateFormat = "Date: {0}";

        private readonly StringBuilder builder = new StringBuilder();

        public void AddLineItem(Line line, decimal amount)
        {
            builder.AppendLine(string.Format(LineItemFormat, 
                line.Quantity, 
                line.Policy.PolicyHolderName, 
                line.Policy.Description, 
                amount.ToString("C")));
        }

        public void AddLineItemPrefix()
        {
            // do nothing. - Nothing necessary for text output.
        }

        public void AddLineItemSuffix()
        {
            // do nothing. - Nothing necessary for text output.
        }

        public void AddReceiptPrefix(string company)
        {
            builder.Append(string.Format(ReceiptPrefixFormat, company, Environment.NewLine));
        }

        public void AddReceiptSuffix(decimal totalAmount, decimal tax, DateTime orderDate)
        {
            builder.AppendLine(string.Format(SubtotalFormat, totalAmount.ToString("C")));
            builder.AppendLine(string.Format(TaxFormat, tax.ToString("C")));
            builder.AppendLine(string.Format(OrderTotalFormat, (totalAmount + tax).ToString("C")));
            builder.Append(string.Format(OrderDateFormat, orderDate.ToString("F")));
        }

        public override string ToString()
        {
            return builder.ToString();
        }
    }

}
