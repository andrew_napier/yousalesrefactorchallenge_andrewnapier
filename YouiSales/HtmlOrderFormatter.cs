﻿using System;
using System.Text;

namespace YouiSales
{
    /// <summary>
    /// Create an instance of this class to render the order in HTML format.
    /// </summary>
    public class HtmlOrderFormatter : IOrderFormatter
    {
        private const string LineItemPrefixFormat = "<ul>";
        private const string LineItemSuffixFormat = "</ul>";
        private const string ReceiptPrefixFormat = "<html><body><h1>Order Receipt for {0}</h1>";
        private const string LineItemFormat = "<li>{0} x {1} {2} = {3}</li>";
        private const string SubtotalFormat = "<h3>Sub-Total: {0}</h3>";
        private const string TaxFormat = "<h3>Tax: {0}</h3>";
        private const string OrderTotalFormat = "<h2>Total: {0}</h2>";
        private const string OrderDateFormat = "<h3>Date: {0}</h3>";
        private const string HtmlTerminatorFormat = "</body></html>";

        private readonly StringBuilder builder = new StringBuilder();

        public void AddLineItem(Line line, decimal amount)
        {
            builder.Append(string.Format(LineItemFormat, 
                line.Quantity, 
                line.Policy.PolicyHolderName, 
                line.Policy.Description, 
                amount.ToString("C")));
        }

        public void AddLineItemPrefix()
        {
            builder.Append(LineItemPrefixFormat);
        }

        public void AddLineItemSuffix()
        {
            builder.Append(LineItemSuffixFormat);
        }

        public void AddReceiptPrefix(string company)
        {
            builder.Append(string.Format(ReceiptPrefixFormat, company));
        }

        public void AddReceiptSuffix(decimal totalAmount, decimal tax, DateTime orderDate)
        {
            builder.Append(string.Format(SubtotalFormat, totalAmount.ToString("C")));
            builder.Append(string.Format(TaxFormat, tax.ToString("C")));
            builder.Append(string.Format(OrderTotalFormat, (totalAmount + tax).ToString("C")));
            builder.Append(string.Format(OrderDateFormat, orderDate.ToString("F")));
            builder.Append(HtmlTerminatorFormat);
        }

        public override string ToString()
        {
            return builder.ToString();
        }
    }
}
