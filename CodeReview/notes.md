# Introduction

I made the mistake of presuming that the code-review would be requested at the point in time of a successful build (ie. running unit-tests)
This meant I started by looking at the classes, rather than attempting to run the  unit-tests as-is.  Once I got to the Order class, I realised
it would be prudent to try running them!  

My approach then changed to "bare minimum to fix tests, then apply some of the feedback" (as per what I'd written on the classes below)

I try to keep my code-review feedback to be non-personal and leave out pronouns such as "You should do this..."

Obviously, in this coding challenge you were keen to provide "a large scope" for what can be suggested for review.  I would expect to see 
this level of code in someone quite junior, if I was asked to mentor them.  If this were real, I'd try to go through the code face to face 
as there's more room for a "compassionate review". 

In my current role, I have SCA tools which would help me spot deficiencies in code-coverage of the unit tests.  Without it (as I am here) I 
realise what a bonus these tools are to have!  I would have liked to have provided more examples of unit tests, but didn't have the time to 
debug the current ones and see where code-coverage was lacking.

The solution I'm providing could be extended further.  I started down a path of using constructor-based dependency injection, but that hasn't 
carried all the way through.  Similar to the `policyPortfolio` I introduced, the current solution could be extended to have a `formatters` 
dictionary passed in, to avoid the construction of the formatters on the fly.



# Details

## Notes on Classes

### Line

- Should the properties of `Line` (`Policy` and `Quantity`) be mutable?  The code-fragment is too small to be sure, but it would seem likely that a `Line` instance should be read-only.  If it were incorrectly input, the `Order` (or some unseen `OrderBuilder`-type class) should facilitate its removal.  
- It is better for code to "fail early" than hide potential problems.  In this case, if a `null` Policy is provided, fail in the constructor of the `Line` class.
```
       public Line(Policy policy, int quantity)
        {
            Policy = policy  ?? throw new ArgumentNullException(nameof(policy));
            ...
```
- A negative `quantity` value is allowed, in case this is used to signify a refund, but a zero quantity should possibly be disallowed. (?)

### Logger

- Singleton classes make for more difficult unit-testing.  It is preferable to inject a logger instance (or interface) into classes that require it, to allow for mocks/substitutions.  
- `Console.Writeline()` will output to STD OUT.  This is presumed to be ok for the purpose of this test...  

### Order

- As mentioned in the `Logger` class, it's preferable to inject the dependencies (i.e. `Logger`)
- There is no consistency with the private member fields of this class.  Camel-case is a good base to work with: `taxRate` and `lines`, starting with lower-case for private members, and upper-case for public/internal. 
- Usual question of the mutable property `Company` and no null/empty verification in the constructor. 
- `Receipt()` and `HTMLReceipt()` have a lot of repeated code.  This duplication should be avoided.
- The logic of the two functions differ unintentionally.(based on Unit-test failures) 



### OrderTest
- The unit-tests do not pass, at this point in time - with multiple issues. (e.g. The for-loop in `HtmlReceipt()` is incorrectly bounded, calculations incorrectly generated, date changes)

### Policy
- In the interests of having a single-responsibility in each class, the constants for policy types would be better off in a separate class or enumerated type.
- These constants, representing a dollar figure (and the `Price` property) would be better represented as `decimal` types to allow for dollar and cents figures.
- The `Price` property is "overloaded" with two responsibilities.  (Dollar figure and type-of).  Altering the property so that it represents the type of policy allows abstraction of the pricing model to a business-layer object.
- Simple validation of constructor arguments should be performed. (as per notes in `Line`)
- The `Policy` object should be immutable as well.



