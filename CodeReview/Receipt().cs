        public string Receipt()
        {
            Logger.Instance.LogInformation("Printing receipt (text version) - Start");

            var totalAmount = 0d;
            var result = new StringBuilder(string.Format("Order Receipt for {0}{1}", Company, Environment.NewLine));
            for (var index = 0; index < _lines.Count; index++)
            {
                var line = _lines[index];
                var thisAmount = 0d;
                if (line.Policy.Price == Policy.Car)
                {
                    if (line.Quantity >= 1)
                        thisAmount += line.Quantity * line.Policy.Price * .9d;
                    else
                        thisAmount += line.Quantity * line.Policy.Price;
                }
                else if (line.Policy.Price == Policy.Motorcycle)
                {
                    if (line.Quantity >= 2)
                        thisAmount += line.Quantity * line.Policy.Price * .8d;
                    else
                        thisAmount += line.Quantity * line.Policy.Price;
                }
                else if (line.Policy.Price == Policy.Home)
                {
                    if (line.Quantity >= 1)
                        thisAmount += line.Quantity * line.Policy.Price * .8d;
                    else
                        thisAmount += line.Quantity * line.Policy.Price;
                }

                result.AppendLine(string.Format("\t{0} x {1} {2} = {3}", line.Quantity, line.Policy.PolicyHolderName, line.Policy.Description, thisAmount.ToString("C")));
                totalAmount += thisAmount;
            }

            result.AppendLine(string.Format("Sub-Total: {0}", totalAmount.ToString("C")));
            var tax = totalAmount * TaxRate;
            result.AppendLine(string.Format("Tax: {0}", tax.ToString("C")));
            result.AppendLine(string.Format("Total: {0}", (totalAmount + tax).ToString("C")));
            result.Append(string.Format("Date: {0}", DateTime.Now.ToString("F")));

            Logger.Instance.LogInformation("Printing receipt (text version) - Finish");

            return result.ToString();
        }
